<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="user_add")
     */

    public function addUser(Request $request)
    {

        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

           // $password= $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
           // $user->setPassword($password);

            $user->setEnabled(true);
            //$user->addRole("ROLE_ADMIN");

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash('success', 'Votre compte à bien été enregistré.');
        }

            return $this->render('security/Registration.html.twig', [
                'form' => $form->createView()
            ]);
        }

}



/*

    public function add_user(Request $request)
    {
       $user = new  User();
       $form = $this ->createForm(RegistrationType::class, $user);
    $form->handleRequest($request);
    if($form->isSubmitted()&& $form->isValid())
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
    }
        return $this->render('security/Registration.html.twig', [
            'form' => $form->createView()
        ]);
    }
}*/
